from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

# urlpatterns = patterns('frontview.views',
#   url(r'^$', 'index'),
#   url(r'^logout/$', 'logout_page'),
# )

# urlpatterns += patterns('',
urlpatterns = patterns('',
  # Examples:
  # url(r'^$', 'bodeguero.views.home', name='home'),
  # url(r'^bodeguero/', include('bodeguero.foo.urls')),

  # Uncomment the admin/doc line below to enable admin documentation:
  # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
  # Uncomment the next line to enable the admin:
  url(r'^admin/', include(admin.site.urls)),
  url(r'^', include('frontview.urls')),
  url(r'^login/', 'django.contrib.auth.views.login'),
)
