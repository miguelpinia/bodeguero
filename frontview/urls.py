from django.conf.urls import patterns, include, url

handler404 = 'frontview.views.my_404_view'
handler500 = 'frontview.views.my_500_view'

urlpatterns = patterns('frontview.views',
  url(r'^$', 'index'),
  url(r'^logout/$', 'logout_page'),
)