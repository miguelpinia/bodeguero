# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import render_to_response 
from django.template import RequestContext
from stock.models import Product, Sale_order, Purchase_order, Purchase, Sale

@login_required(login_url='/login/')
def index(request):
  product_list = Product.objects.all()
  return render_to_response(
    'index.html',
    {'product_list': product_list},
    context_instance=RequestContext(request)
  )

def logout_page(request):
  logout(request)
  return(HttpResponseRedirect('/'))

def my_404_view(request):
  return(render_to_response('404.html'))

def my_500_view(request):
  return(render_to_response('500.html'))