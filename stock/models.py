from django.db import models
from django.utils.translation import gettext_lazy as _

class Employee(models.Model):
  first_name = models.CharField(max_length=50, verbose_name=_('first name'))
  last_name = models.CharField(max_length=50, verbose_name=_('last name'))
  id_company = models.PositiveIntegerField(null=True, unique=True, blank=True, verbose_name=_('company id'))
  def __unicode__(self):
    return '%s %s' % (self.first_name, self.last_name)
  class Meta:
    verbose_name=_('employee')
    ordering = ['last_name']

class Storage_unit(models.Model):
  description = models.CharField(max_length=80, verbose_name=_('description'))
  def __unicode__(self):
    return self.description
  class Meta:
    verbose_name=_('storage unit')
    verbose_name_plural=_('storage units')
    ordering = ['description']

class Category(models.Model):
  description = models.CharField(max_length=20, verbose_name=_('description'))
  def __unicode__(self):
    return self.description
  class Meta:
    verbose_name=_('category')
    verbose_name_plural=_('categories')
    ordering = ['description']

class Purchase(models.Model):
  who_receives = models.ForeignKey(Employee)
  timestamp = models.DateTimeField(auto_now_add=True, verbose_name=_('date received'))
  def purchase_detail(self):
    purchase_set = self.purchase_order_set.all()
    if purchase_set.count() == 1:
      product = purchase_set.values('id_product')
      return Product.objects.filter(purchase_order__id_purchase=self.id).get()
    else:
      return _('Multiple items in purchase %s') % self.id
  def __unicode__(self):
    return 'Purchase Order: %s' % self.id
  class Meta:
    get_latest_by = 'timestamp'
    verbose_name=_('purchase')
  purchase_detail.short_description = _('purchase details')

class Sale(models.Model):
  who_receives = models.ForeignKey(Employee, verbose_name=_('who receives'))
  ticket_id = models.IntegerField(null=True, blank=True, verbose_name=_('ticket id'))
  who_asks = models.CharField(max_length=100, null=True, blank=True, verbose_name=_('who asks'))
  was_delivered = models.NullBooleanField(verbose_name=_('was delivered'))
  timestamp = models.DateTimeField(auto_now_add=True, verbose_name=_('date delivered'))
  def sale_detail(self):
    sale_set = self.sale_order_set.all()
    if sale_set.count() == 1:
      product = sale_set.values('id_product')
      return Product.objects.filter(sale_order__id_sale=self.id).get()
    else:
      return _('Multiple items in sale %s' % self.id)
  def __unicode__(self):
    return 'Sale Order: %s' % self.id
  class Meta:
    get_latest_by = 'timestamp'
    verbose_name=_('sale')
  sale_detail.short_description = _('sale details')

class Product(models.Model):
  barcode = models.CharField(max_length=30, unique=True, verbose_name=_('barcode'))
  description = models.CharField(max_length=200, verbose_name=_('description'))
  brand = models.CharField(max_length=20, null=True, blank=True, verbose_name=_('brand'))
  type = models.CharField(max_length=35, null=True, blank=True, verbose_name=_('model'))
  unit = models.CharField(max_length=10, verbose_name=_('unit'))
  category = models.ForeignKey(Category)
  storage_unit = models.ForeignKey(Storage_unit)
  ideal_stock = models.PositiveIntegerField(default=0, verbose_name=_('ideal stock'))
  min_stock = models.PositiveIntegerField(default=0, verbose_name=_('minimimum stock'))
  def current_stock(self):
    in_orders = self.purchase_order_set.all()
    out_orders = self.sale_order_set.all()
    purchased = in_orders.aggregate(models.Sum('qty_units'))['qty_units__sum']
    sold = out_orders.aggregate(models.Sum('qty_units'))['qty_units__sum']
    try:
      return purchased-sold
    except:
      if not purchased: purchased = 0
      if not sold: sold = 0
      return purchased-sold
  def __unicode__(self):
    return '%s: %s' % (self.category, self.description)
  class Meta:
    verbose_name=_('product')
    ordering = ['category', 'description']

class Purchase_order(models.Model):
  id_purchase = models.ForeignKey(Purchase)
  id_product = models.ForeignKey(Product)
  qty_units = models.PositiveIntegerField()
  unit_price = models.DecimalField(decimal_places=2, max_digits=15, verbose_name=_('unit price'))
  def __unicode__(self):
    return '%s Product: %s' % (self.id_purchase, self.id_product)
  class Meta:
    verbose_name = _('purchase order')

class Sale_order(models.Model):
  id_sale = models.ForeignKey(Sale)
  id_product = models.ForeignKey(Product)
  qty_units = models.PositiveIntegerField(verbose_name=_('qty unit'))
  def __unicode__(self):
    return '%s Product: %s' % (self.id_sale, self.id_product)
  class Meta:
    verbose_name= _('sale order')
    verbose_name_plural = _('sale orders')
