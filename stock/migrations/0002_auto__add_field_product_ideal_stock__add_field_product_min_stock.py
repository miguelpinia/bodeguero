# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Product.ideal_stock'
        db.add_column('stock_product', 'ideal_stock',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Product.min_stock'
        db.add_column('stock_product', 'min_stock',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Product.ideal_stock'
        db.delete_column('stock_product', 'ideal_stock')

        # Deleting field 'Product.min_stock'
        db.delete_column('stock_product', 'min_stock')


    models = {
        'stock.category': {
            'Meta': {'object_name': 'Category'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'stock.employee': {
            'Meta': {'object_name': 'Employee'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_company': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'stock.product': {
            'Meta': {'object_name': 'Product'},
            'barcode': ('django.db.models.fields.IntegerField', [], {'unique': 'True'}),
            'brand': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Category']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ideal_stock': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'min_stock': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'storage_unit': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Storage_unit']"}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'stock.purchase': {
            'Meta': {'object_name': 'Purchase'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'who_receives': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Employee']"})
        },
        'stock.purchase_order': {
            'Meta': {'object_name': 'Purchase_order'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Product']"}),
            'id_purchase': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Purchase']"}),
            'qty_units': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'unit_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'})
        },
        'stock.sale': {
            'Meta': {'object_name': 'Sale'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ticket_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'was_delivered': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'who_asks': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'who_receives': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Employee']"})
        },
        'stock.sale_order': {
            'Meta': {'object_name': 'Sale_order'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Product']"}),
            'id_sale': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Sale']"}),
            'qty_units': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'stock.storage_unit': {
            'Meta': {'object_name': 'Storage_unit'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['stock']