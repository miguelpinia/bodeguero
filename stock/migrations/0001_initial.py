# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Employee'
        db.create_table('stock_employee', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('id_company', self.gf('django.db.models.fields.PositiveIntegerField')(unique=True, null=True, blank=True)),
        ))
        db.send_create_signal('stock', ['Employee'])

        # Adding model 'Storage_unit'
        db.create_table('stock_storage_unit', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=80)),
        ))
        db.send_create_signal('stock', ['Storage_unit'])

        # Adding model 'Category'
        db.create_table('stock_category', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('stock', ['Category'])

        # Adding model 'Purchase'
        db.create_table('stock_purchase', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('who_receives', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stock.Employee'])),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('stock', ['Purchase'])

        # Adding model 'Sale'
        db.create_table('stock_sale', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('who_receives', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stock.Employee'])),
            ('ticket_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('who_asks', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('was_delivered', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('stock', ['Sale'])

        # Adding model 'Product'
        db.create_table('stock_product', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('barcode', self.gf('django.db.models.fields.IntegerField')(unique=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('brand', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('unit', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stock.Category'])),
            ('storage_unit', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stock.Storage_unit'])),
        ))
        db.send_create_signal('stock', ['Product'])

        # Adding model 'Purchase_order'
        db.create_table('stock_purchase_order', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('id_purchase', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stock.Purchase'])),
            ('id_product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stock.Product'])),
            ('qty_units', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('unit_price', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=2)),
        ))
        db.send_create_signal('stock', ['Purchase_order'])

        # Adding model 'Sale_order'
        db.create_table('stock_sale_order', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('id_sale', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stock.Sale'])),
            ('id_product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stock.Product'])),
            ('qty_units', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('stock', ['Sale_order'])


    def backwards(self, orm):
        # Deleting model 'Employee'
        db.delete_table('stock_employee')

        # Deleting model 'Storage_unit'
        db.delete_table('stock_storage_unit')

        # Deleting model 'Category'
        db.delete_table('stock_category')

        # Deleting model 'Purchase'
        db.delete_table('stock_purchase')

        # Deleting model 'Sale'
        db.delete_table('stock_sale')

        # Deleting model 'Product'
        db.delete_table('stock_product')

        # Deleting model 'Purchase_order'
        db.delete_table('stock_purchase_order')

        # Deleting model 'Sale_order'
        db.delete_table('stock_sale_order')


    models = {
        'stock.category': {
            'Meta': {'object_name': 'Category'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'stock.employee': {
            'Meta': {'object_name': 'Employee'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_company': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'stock.product': {
            'Meta': {'object_name': 'Product'},
            'barcode': ('django.db.models.fields.IntegerField', [], {'unique': 'True'}),
            'brand': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Category']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'storage_unit': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Storage_unit']"}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'stock.purchase': {
            'Meta': {'object_name': 'Purchase'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'who_receives': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Employee']"})
        },
        'stock.purchase_order': {
            'Meta': {'object_name': 'Purchase_order'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Product']"}),
            'id_purchase': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Purchase']"}),
            'qty_units': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'unit_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'})
        },
        'stock.sale': {
            'Meta': {'object_name': 'Sale'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ticket_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'was_delivered': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'who_asks': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'who_receives': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Employee']"})
        },
        'stock.sale_order': {
            'Meta': {'object_name': 'Sale_order'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Product']"}),
            'id_sale': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stock.Sale']"}),
            'qty_units': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'stock.storage_unit': {
            'Meta': {'object_name': 'Storage_unit'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['stock']