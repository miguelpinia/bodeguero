# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Product.type'
        db.alter_column(u'stock_product', 'type', self.gf('django.db.models.fields.CharField')(max_length=35, null=True))

    def backwards(self, orm):

        # Changing field 'Product.type'
        db.alter_column(u'stock_product', 'type', self.gf('django.db.models.fields.CharField')(max_length=20, null=True))

    models = {
        u'stock.category': {
            'Meta': {'ordering': "['description']", 'object_name': 'Category'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'stock.employee': {
            'Meta': {'ordering': "['last_name']", 'object_name': 'Employee'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_company': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'stock.product': {
            'Meta': {'ordering': "['category', 'description']", 'object_name': 'Product'},
            'barcode': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'brand': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stock.Category']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ideal_stock': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'min_stock': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'storage_unit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stock.Storage_unit']"}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '35', 'null': 'True', 'blank': 'True'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'stock.purchase': {
            'Meta': {'object_name': 'Purchase'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'who_receives': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stock.Employee']"})
        },
        u'stock.purchase_order': {
            'Meta': {'object_name': 'Purchase_order'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stock.Product']"}),
            'id_purchase': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stock.Purchase']"}),
            'qty_units': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'unit_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'})
        },
        u'stock.sale': {
            'Meta': {'object_name': 'Sale'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ticket_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'was_delivered': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'who_asks': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'who_receives': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stock.Employee']"})
        },
        u'stock.sale_order': {
            'Meta': {'object_name': 'Sale_order'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stock.Product']"}),
            'id_sale': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stock.Sale']"}),
            'qty_units': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'stock.storage_unit': {
            'Meta': {'ordering': "['description']", 'object_name': 'Storage_unit'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['stock']