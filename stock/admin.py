from stock.models import *
from django.contrib import admin

# Inlines

class PurchaseOrderInline(admin.TabularInline):
  model = Purchase_order
  extra = 1

class SaleOrderInline(admin.TabularInline):
  model = Sale_order
  extra = 1

# Admin Panels

class EmployeeAdmin(admin.ModelAdmin):
  fieldsets = [
    (None,                {'fields': ['first_name', 'last_name']}),
    ('Extra information', {'fields': ['id_company']}),
  ]

class PurchaseAdmin(admin.ModelAdmin):
  fieldsets = [
    ('Purchase Information', {'fields': ['who_receives']}),
  ]
  inlines = [PurchaseOrderInline]
  list_display = ('id', 'who_receives', 'purchase_detail', 'timestamp')
  list_filter = ['timestamp']
  search_fields = ['who_receives__first_name', 'who_receives__last_name', 'who_receives__id_company']

class SaleAdmin(admin.ModelAdmin):
  fieldsets = [
    ('Order Information', {'fields': ['who_receives', 'ticket_id', 'who_asks', 'was_delivered']}),
  ]
  inlines = [SaleOrderInline]
  list_display = ('id', 'who_receives', 'who_asks', 'was_delivered', 'sale_detail', 'timestamp')
  list_filter = ['was_delivered', 'timestamp']

class ProductAdmin(admin.ModelAdmin):
  list_display = ('description', 'category', 'barcode')
  list_filter = ['category']
  search_fields = ['category__description', 'description', 'brand', 'type', 'barcode']

admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Storage_unit)
admin.site.register(Category)
admin.site.register(Sale, SaleAdmin)
admin.site.register(Purchase, PurchaseAdmin)
admin.site.register(Product, ProductAdmin)
